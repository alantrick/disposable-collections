/**
 * A write-only collection of disposable values that is disposable too.
 *
 * This is only really useful if you don't want to name/reference the
 * things, otherwise disposableObject is better.
 *
 * @param items
 */
export function disposableCollection(items: Iterable<Disposable>): Disposable {
  return {
    [Symbol.dispose]() {
      [...items].forEach((item) => item[Symbol.dispose]());
    },
  };
}

/**
 * An object of disposable values that is disposable too.
 *
 * The object will dispose of all its members when out of scope.
 *
 * @param items
 */
export function disposableObject<T extends Record<string, Disposable>>(
  object: T
): T & Disposable {
  const entries = Object.entries(object);
  return {
    ...object,
    [Symbol.dispose]() {
      entries.forEach(([, value]) => value[Symbol.dispose]());
    },
  };
}
