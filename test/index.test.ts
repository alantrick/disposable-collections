import { disposableCollection, disposableObject } from "../src";

// While typescript is happy to convert the "using" keyword into something
// functional, but does not want to do any actual polyfill-ing.

// In general, it's expected that you might use something like core-js or
// babel to provide polyfills. In this case, however, that's over-kill:
// all we need here is Symbol.dispose, and we can get it by just doing this:
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/

class Item<T extends string> implements Disposable {
  disposed = false;
  name: T;
  constructor(name: T) {
    this.name = name;
  }

  [Symbol.dispose]() {
    this.disposed = true;
  }
}

const abc = () => {
  const a = new Item("a"),
    b = new Item("b"),
    c = new Item("c");
  return { a, b, c };
};

describe("disposableCollection", () => {
  it("disposes on scope end", () => {
    const escaped = (() => {
      const { a, b, c } = abc();
      using _ = disposableCollection([a, b, c]);
      expect(a.disposed).toBe(false);
      expect(b.disposed).toBe(false);
      expect(c.disposed).toBe(false);
      return { a, b, c };
    })();
    expect(escaped.a.disposed).toBe(true);
    expect(escaped.b.disposed).toBe(true);
    expect(escaped.a.disposed).toBe(true);
  });
});
describe("disposableObject", () => {
  it("disposes on scope end", () => {
    const escaped = (() => {
      const { a, c, b } = abc();
      using obj = disposableObject({ a, b, c });
      expect(obj.a.disposed).toBe(false);
      expect(obj.b.disposed).toBe(false);
      expect(obj.c.disposed).toBe(false);
      return obj
    })();
    expect(escaped.a.disposed).toBe(true);
    expect(escaped.b.disposed).toBe(true);
    expect(escaped.a.disposed).toBe(true);
  });
});
